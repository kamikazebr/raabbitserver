var mongoose = require('mongoose');
var UserSchema = new mongoose.Schema({
    name: String
    , email: String
    , password: String
    , displayName: String
    , admin_channels: Array
});
mongoose.model('User', UserSchema);