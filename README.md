# Raabbit Server #

The Raabbit Server made using 

* [Node.js](https://nodejs.org)
* [MongoDB](https://www.mongodb.com/)
* [Couch Sync Gateway](https://github.com/couchbase/sync_gateway) using in-memory database

## What is this repository for?

* Used to communicate with the [Raabbit Android app](https://bitbucket.org/kamikazebr/raabbit/)
* Version 0.0.1

### How do I get set up? ###
* Install and run [MongoDB](https://www.mongodb.com/)
* Download and run [Couch Sync Gateway](https://github.com/couchbase/sync_gateway)
* Clone repo and run `npm install`
* Using [forever](https://github.com/foreverjs/forever) just run `forever start ./bin/www`

## Who do I talk to?

* Repo owner Felipe Novaes F. da Rocha (Phillip Kamikaze) [windholyghost@gmail.com](mailto://windholyghost@gmail.com)

## License
	Copyright 2016 Felipe Novaes F. da Rocha

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

   		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
