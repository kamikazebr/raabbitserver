var express = require('express')
    , router = express.Router()
    , restler = require('restler')
    , util = require('util')
    , mongoose = require('mongoose')
    , User = mongoose.model('User')
    , bodyParser = require('body-parser')
    , methodOverride = require('method-override'); //used to manipulate POST


var sync_url = "http://localhost:4985/raabbit/"
var getListUsers = sync_url + "_user/"
var getUser = sync_url + "_user/{name}"
var postNewUser = sync_url + "_user/"
var postNewSession = sync_url + "_session"
var putNewUser = sync_url + "_user/{name}" //createOrUpdate
var deleteNewUser = sync_url + "_user/{name}" //createOrUpdate
    /* GET users listing. */
router.get('/', function (req, res, next) {
    res.send('respond with a resource');
});

router.post('/', function (req, res, next) {
    var user = req.body;
    var tempName = user.name;
    user.name = user.email;
    user.displayName = tempName;
    user.admin_channels = [].concat(user.email);

    User.find({
            'email': user.email
        })
        .limit(1)
        .exec(function (err, result) {
            if (err) {
                console.log(err);
                res.status(400).json({
                    status: false
                    , reason: 'Ocorreu um erro'
                });
            } else if (result.length > 0) {
                console.log(result);
                res.status(409).json({
                    reason: 'JA_CADASTRADO'
                });
            } else {
                //Não existe, entao adiciona
                User.findOneAndUpdate({
                    email: user.email
                }, user, {
                    upsert: true
                    , setDefaultsOnInsert: true
                }, function (er, d) {
                    if (er) {
                        console.log(er);
                        res.status(404).json(err);
                    } else {
                        res.status(201).json({
                            status: true
                        });
                    }
                });
            }
        });
});
//TODO ON ERROR

router.post('/login', function (req, res, next) {
    User.find({
            'email': req.body.email
            , 'password': req.body.password
        })
        .limit(1)
        .exec(function (err, result) {
            if (err) {
                console.log(err);
                res.status(400).json({
                    status: false
                    , reason: 'Ocorreu um erro'
                });
            } else if (result.length > 0) {
                console.log(result);
                res.status(200).json({
                    status: true
                });
                //                restler.postJson(postNewSession, {
                //                    'name': result[0].email
                //                }).on('complete', function (data, response) {
                //                    console.log(data);
                //                    var status = response.statusCode === 200;
                //                    res.status(response.statusCode).json(data);
                //                }); //TODO ON ERROR
            } else {
                res.status(401).send({
                    reason: 'NAO_AUTORIZADO'
                });
            }
        });
});


//            restler.postJson(postNewUser, user).on('complete', function (data, response) {
//                console.log(data);
//                var status = true;
//                if (data.hasOwnProperty('error')) {
//                    status = false;
//                }
//                delete d.password;
//                res.status(response.statusCode).json({
//                    'status': status
//                    , 'd': d
//                }); //TODO ON ERROR
//            });

module.exports = router;